export function qs(selector: string) {
	return document.querySelector(selector);
}

export function qsAll(selector: string) {
	return document.querySelectorAll(selector);
}

export function dynamicListener(type: string, selector: string, method: Function) {
	if(qsAll(selector).length > 0) {
		[].forEach.call(qsAll(selector), (element: Element) => {
			document.addEventListener(type, (event) => {
				if(event.target === element) method(event);
			});
		});
	} else {
		document.addEventListener(type, (event) => {
			if(event.target === qs(selector)) method(event);
		});
	}
}