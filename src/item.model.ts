interface Item {
	id: number,
	title: string,
	completed?: boolean,
	important?: boolean;
	[key: string]: string | number | boolean;
	[index: number]: string | number | boolean;
}

class Item implements Item {

	public id: number;
	public title: string;
	public completed?: boolean;
	public important?: boolean;

	constructor(id: number, title: string, completed: boolean = false, important: boolean = false) {
		this.id = id;
		this.title = title;
		this.completed = completed;
		this.important = important;
	}
}

export default Item