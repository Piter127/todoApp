import Store from './store';
import View from './view';
import Item from './item.model';

class Controller {

	public store: Store;
	public view: View;

	public activeFilter: any = 'all';
	public filteredTodosCount: number;

	[key: string]: any;
	
	constructor(store: Store, view: View) {
		this.store = new Store();
		this.view = new View(this.store);

		this.setActiveFilter();

		this.bindActions();
	}

	setActiveFilter(name: string = 'all') {
		this.activeFilter = this.view.views.find((view: any) => view.filter === (name));

		return this._filter();
	}

	_filter() {
		this.view.views = this.view.views.map((view: any) => ({...view, active: (view.name !== this.activeFilter.name) ? false : true}));
		
		const todos = this.getTodos();
		this.filteredTodosCount = todos.length;

		this.view.renderView(this.activeFilter, this.filteredTodosCount);
		this.view.renderTodos(todos);
		this.view.bindEvents();
		this.bindActions();
	}

	getTodos() {
    	return this[`${this.activeFilter.filter}Todos`]();
  	}

  	allTodos() {
		return this.store.todos;
	}

	importantTodos() {
		return this.store.todos.filter((todo: Item) => todo.important === true);
	}

	activeTodos() {
		return this.store.todos.filter((todo: Item) => todo.completed === false);
	}

	completedTodos() {
		return this.store.todos.filter((todo: Item) => todo.completed === true);
	}

	addItem(title: string) {
		const todo = new Item(
			new Date().getTime(), 
			title
		);

		let todos = this.store.insert(todo);
		todos = this.getTodos();

		this.view.renderTodos(todos);
		this.view.hideModal();
		this.bindActions();
	}

	toggleState(id: number, state: string, value: boolean) {
		let todos = this.store.todos;

		todos.forEach((todo, i) => {
			if(todo.id === id) todos[i][state] = value;
		});

		this.store.todos = todos
		todos = this.getTodos();

		this.view.renderTodos(todos);
		this.bindActions();
	}

	bindActions() {
		this.view.bindAddItem(this.addItem.bind(this));
		this.view.bindToggleState(this.toggleState.bind(this));
		this.view.bindFilter(this.setActiveFilter.bind(this));
	}
}

export default Controller