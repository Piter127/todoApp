import Item from './item.model';
import Store from './store';

// templates
import * as header from './handlebars/templates/header.hbs';
import * as main from './handlebars/templates/main.hbs';
import * as newItemModal from './handlebars/templates/new-item-modal.hbs';
import * as todosList from './handlebars/templates/todo-list.hbs';

import { qs, qsAll, dynamicListener } from './helpers';
import './Element.prototype';

import * as views from './data/views.json';

class View {

	private static ENTER_KEY: number = 13;
	private static ESCAPE_KEY: number = 27;

	private todosWrapper: Element = qs('.js--todo-list');
	private $header: Element = qs('.header');
	private $main: Element = qs('.main');

	public store: Store;

	public views: any;

	constructor(store: Store) {
		this.store = store;
		this.views = views;
	}

	renderView(activeFilter: any, filteredTodosCount: number) {
		const data = {
			views: this.views,
			activeView: activeFilter,
			todosCount: filteredTodosCount
		}

		this.$header.innerHTML = header();
		this.$main.innerHTML = main(data);
	}

	renderTodos(todos: Item[]) {
		const todoListTemplate = todosList({todos});

		qs('.js--todo-list').innerHTML = todoListTemplate;
	}

	showModal(template: Function) {
		qs('body').insertHTMLBefore(template());
		(<HTMLElement>qs('.modal'))
			.show()
			.querySelector('input').focus();
		this.bindCloseModal();
	}

	hideModal() {
		qs('.modal').hide().remove();
	}

	bindCloseModal() {
		document.addEventListener('keyup', (event) => {
			if(event.keyCode === View.ESCAPE_KEY) this.hideModal();
		});

		qs('.modal').addEventListener('click', (event) => {
			if(<Element>event.target === qs('.modal')) this.hideModal();
		});
	}

	bindAddItem(handler: any) {
		dynamicListener('submit', '.js--submit-form', (event: Event) => {
			event.preventDefault();

			const title = (<HTMLInputElement>qs('#title')).value.trim();

			if(title) handler(title);
		});
	}

	bindToggleState(handler: any) {
		dynamicListener('click', '.js--toggle-state', (event: Event) => {
			const todoId = parseInt((<HTMLElement>(<Element>event.target).closest('tr')).dataset.id);
			const name = (<HTMLInputElement>event.target).getAttribute('name');
			const value = (<HTMLInputElement>event.target).checked;

			if(todoId) handler(todoId, name, value);
		});
	}

	bindFilter(handler: any) {
		dynamicListener('click', '.js--filter', (event: Event) => {
			const filter = (<HTMLElement>event.target).dataset.filter;
			if(filter) handler(filter); 
		});
	}

	bindEvents() {
		qs('.js--modal-new-todo').addEventListener('click', () => this.showModal(newItemModal));
	}	
}

export default View