import Item from './item.model';

class Store {

	private _todos: Item[];
	private static STORE_NAME: string = 'todos';
	
	constructor() {}

	get todos(): Item[] {
		return (this._todos || JSON.parse(localStorage.getItem(Store.STORE_NAME) || '[]'));
	}

	set todos(todos: Item[]) {
		localStorage.setItem(Store.STORE_NAME, JSON.stringify(this._todos = todos));
	}

	insert(todo: Item) {
		const todos = this.todos;
		todos.push(todo);
		this.todos = todos;

		return this.todos;
	}
}

export default Store;