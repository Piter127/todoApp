import Store from './store';
import View from './view';
import Controller from './controller';

class TodoApp {
	public store: Store;
	public view: View;
	public controller: Controller;

	constructor() {
		this.store = new Store();
		this.view = new View(this.store);
		this.controller = new Controller(this.store, this.view);
	}
}

const todoApp = new TodoApp();