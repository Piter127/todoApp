export {}

declare global {
	interface Element {
		show(): Element;
		hide(): Element;
		insertHTMLBefore(html: string): void;
		insertHTMLAfter(html: string): void;
	}
}

Element.prototype.show = function(): Element {
	this.style.display = 'block';
	return this;
}

Element.prototype.hide = function(): Element {
	this.style.display = 'none';
	return this;
}

Element.prototype.insertHTMLBefore = function(html: string): void {
	this.insertAdjacentHTML('beforeend', html);
}

Element.prototype.insertHTMLAfter = function(html: string): void {
	this.insertAdjacentHTML('afterbegin', html);
}