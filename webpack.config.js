var path = require('path');
var webpack = require('webpack');

module.exports = {
	entry: './src/index.ts',
	resolve: {
		extensions: ['.webpack.js', '.web.js', '.ts', '.js', '.hbs'],
	},
	output: {
		filename: 'bundle.js',
		path: path.resolve(__dirname, 'dist')
	},
	module: {
        rules: [
            {
                test: /\.ts$/,
                use: [{
                    loader: 'ts-loader'
                }]
            },
            {
                test: /\.hbs$/,
                use: [{
                    loader: 'handlebars-loader',
                    options: {
                        helperDirs: [
                            __dirname + "/src/handlebars-helpers"
                        ]
                    }
                }],
            },
            {
                test: /\.json$/,
                use: [{
                    loader: 'json-loader'
                }]
            }
        ]
    },
	devtool: 'source-map',
}